package com.example.movierepo.activity;

import android.os.Bundle;
import android.util.Log;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.example.movierepo.R;
import com.example.movierepo.adapter.MoviesAdapter;
import com.example.movierepo.model.Movie;
import com.example.movierepo.model.MovieResponse;
import com.example.movierepo.rest.MovieApiService;
import com.example.movierepo.rest.RetrofitFactory;

import java.util.List;

import io.reactivex.SingleObserver;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.Disposable;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;

public class MainActivity extends AppCompatActivity {
    private static final String TAG = MainActivity.class.getSimpleName();
    public static final String BASE_URL = "http://api.themoviedb.org/3/";
    private static Retrofit retrofit;
    private String topRated = null;
    private RecyclerView recyclerView = null;
    SingleObserver<MovieResponse> response;

    // insert your themoviedb.org API KEY here
    private final static String API_KEY = RetrofitFactory.getApiKey();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setHasFixedSize(true);
        recyclerView.setLayoutManager(new LinearLayoutManager(this));
        connectAndGetApiData();
    }

    // This method create an instance of Retrofit
// set the base url
    public void connectAndGetApiData(){
      retrofit = RetrofitFactory.getRetrofit();

       /* if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }*/
        MovieApiService movieApiService = retrofit.create(MovieApiService.class);
        //Call<MovieResponse> call = movieApiService.getTopRatedMovies(API_KEY);

              response = movieApiService.getTopRatedMovies(API_KEY)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeWith(new SingleObserver<MovieResponse>() {
                    @Override
                    public void onSubscribe(Disposable d) {

                    }

                    @Override
                    public void onSuccess(MovieResponse response) {
                        List<Movie> movies = response.getResults();
                        recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
                        Log.d(TAG, "Number of movies received: " + movies.size());
                    }


                    @Override
                    public void onError(Throwable e) {

                    }
                });

       /* call.enqueue(new Callback<MovieResponse>() {
            @Override
            public void onSuccess() {

            }

            @Override
            public void onError(Exception e) {
               // Log.e(TAG, throwable.toString());
            }

            @Override
            public void onResponse(Call<MovieResponse> call, Response<MovieResponse> response) {
                List<Movie> movies = response.body().getResults();
                recyclerView.setAdapter(new MoviesAdapter(movies, R.layout.list_item_movie, getApplicationContext()));
                Log.d(TAG, "Number of movies received: " + movies.size());
            }
            @Override
            public void onFailure(Call<MovieResponse> call, Throwable throwable) {
                Log.e(TAG, throwable.toString());
            }
        });*/
    }
}
