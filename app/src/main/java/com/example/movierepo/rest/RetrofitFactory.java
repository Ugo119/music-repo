package com.example.movierepo.rest;

import retrofit2.Retrofit;
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;


public class RetrofitFactory {
    private static final String BASE_URL = "http://api.themoviedb.org/3/";
    private static final String IMAGE_URL_BASE_PATH = "http://image.tmdb.org/t/p/w342//";
    private static final String API_KEY = "c901ba9b8eb14385defba04a48be55a6";
    private static Retrofit retrofit;;
    private RetrofitFactory(){}
    public static Retrofit getRetrofit(){
        if(retrofit == null){
            retrofit = new Retrofit.Builder()
                    .baseUrl(getBaseUrl())
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build();
        }
        return retrofit;
    }
    public static String getApiKey(){
        return API_KEY;
    }

    public static String getBaseUrl(){
        return BASE_URL;
    }

    public static String getImageUrlBasePath(){
        return IMAGE_URL_BASE_PATH;
    }

}
