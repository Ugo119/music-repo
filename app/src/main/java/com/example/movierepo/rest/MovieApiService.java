package com.example.movierepo.rest;

import com.example.movierepo.model.MovieResponse;

import io.reactivex.Single;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface MovieApiService {
  /*  @GET("movie/top_rated")
    Call<MovieResponse> getTopRatedMovies(@Query("api_key") String apiKey);*/

    @GET("movie/top_rated")
    Single<MovieResponse> getTopRatedMovies(@Query("api_key") String apiKey);
}
